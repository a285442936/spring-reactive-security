package com.security.main.gateway.route.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
public class PreGateFilterFactory extends AbstractGatewayFilterFactory<PreGateFilterFactory.Config> {

    public PreGateFilterFactory() {
        super(Config.class);
    }

    public GatewayFilter apply() {
        return apply(o -> {
        });
    }
    @Override
    public GatewayFilter apply(Config config) {

        return new GatewayFilter() {
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
                log.info(this.getClass().getSimpleName() + " apply");
                ServerHttpRequest request = exchange.getRequest();
//                request.getHeaders().add("x-auth-ok","ok");
                ServerHttpRequest.Builder builder = request.mutate();
                builder.header("x-auth-ok", "ok");

                return chain.filter(exchange);

            }
        };
    }

    static class Config{

    }
}
