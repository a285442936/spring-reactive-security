package com.security.main.gateway.security.auth;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
public class JwtUser {
    private Long userId;
    private String username;
    private Collection<?> authorities;
}
