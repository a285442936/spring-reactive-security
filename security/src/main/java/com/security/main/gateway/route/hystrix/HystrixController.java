package com.security.main.gateway.route.hystrix;

import com.google.common.collect.Maps;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HystrixController {
    public static final String FALLBACK_URI = "geteway_hystrix_api_fallback";

    @GetMapping(value = FALLBACK_URI)
    public Mono<Map<String, String>> fallback(ServerWebExchange exchange) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
        HashMap<String, String> objectObjectHashMap = Maps.newHashMap();
        objectObjectHashMap.put("code","500");
        objectObjectHashMap.put("msg", "internal error");
        return Mono.just(objectObjectHashMap);
    }
}
