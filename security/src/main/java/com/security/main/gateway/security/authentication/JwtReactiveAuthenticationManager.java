package com.security.main.gateway.security.authentication;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.http.AccessTokenRequiredException;
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class JwtReactiveAuthenticationManager implements ReactiveAuthenticationManager {


    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        log.info("JwtReactiveAuthenticationManager authenticate exec");
        if (false) {
//            throw new RuntimeException();
            return Mono.error(new AccessTokenRequiredException(new BaseOAuth2ProtectedResourceDetails()));
        }
//        if (true) {
//            throw new AccessDeniedException("用户无权限");
//        }
        return Mono.just(new UsernamePasswordAuthenticationToken(null, true));
    }
}
