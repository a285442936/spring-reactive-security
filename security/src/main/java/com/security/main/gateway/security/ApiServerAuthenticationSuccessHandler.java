package com.security.main.gateway.security;

import com.security.main.gateway.security.auth.JwtClaim;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.security.web.server.authentication.ServerAuthenticationSuccessHandler;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
public class ApiServerAuthenticationSuccessHandler implements ServerAuthenticationSuccessHandler {
    @Override
    public Mono<Void> onAuthenticationSuccess(WebFilterExchange webFilterExchange, Authentication authentication) {
        log.info(this.getClass().getSimpleName() + " onAuthenticationSuccess");
        ServerWebExchange exchange = webFilterExchange.getExchange();
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        Object details = authentication.getDetails();
        ServerHttpRequest.Builder mutate = request.mutate();
        if (details != null && ((JwtClaim) details).isUser()) {
            //刷新token
        }
        ServerHttpRequest.Builder builder = request.mutate().header("hahaha", "heiheihei");
        return webFilterExchange.getChain().filter(exchange.mutate().request(builder.build()).build());
    }
}
