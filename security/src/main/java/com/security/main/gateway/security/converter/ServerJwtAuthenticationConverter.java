package com.security.main.gateway.security.converter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.authentication.ServerHttpBasicAuthenticationConverter;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
public class ServerJwtAuthenticationConverter extends ServerHttpBasicAuthenticationConverter {


    @Override
    public Mono<Authentication> convert(ServerWebExchange exchange) {
        log.info(this.getClass().getSimpleName() + "convert");
        ServerHttpRequest request = exchange.getRequest();
        List<String> authorization = request.getHeaders().get("authorization");
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(authorization, null);
        String path = request.getURI().getPath();
        HttpMethod method = request.getMethod();

        token.setDetails(true);
        return Mono.just(token);



//        return super.convert(exchange);
    }
}
