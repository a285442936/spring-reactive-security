package com.security.main.gateway.route.config;

import com.security.main.gateway.route.filter.AfterGateFilterFactory;
import com.security.main.gateway.route.filter.PreGateFilterFactory;
import com.security.main.gateway.route.hystrix.HystrixController;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

@Configuration
public class RoutesConfig {

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/order/**")
                        .filters(f -> f.retry(c -> c.setMethods(HttpMethod.GET).setStatuses(HttpStatus.NOT_FOUND).setRetries(1))
                                .filter(new PreGateFilterFactory().apply())
                                .filter(new AfterGateFilterFactory().apply())
                                .stripPrefix(1)
                                .hystrix(c ->{
                                    c.setName("hystrix")
                                            .setFallbackUri("forward:/"+HystrixController.FALLBACK_URI);
                                })

                        ).uri("lb://order")
                )
                .build();
    }
}
