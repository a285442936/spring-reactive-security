package com.security.main.gateway.security.auth;

import lombok.Data;
import org.springframework.security.jwt.Jwt;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;
@Data
public class JwtClaim implements Serializable {
    public JwtClaim() {
    }
    public JwtClaim(Sub sub, Client client, JwtUser jwtUser) {
        this.sub = sub;
        this.client = client;
        this.user = jwtUser;
    }

    /**
     * jwttoken端剪发时间 如果国旗时间到了一半进行刷行
     */
    private LocalDateTime iat;
    /**
     * 国旗事件
     */
    private LocalDateTime exp;
    private Sub sub;
    private Client client;
    private JwtUser user;

    public enum Sub {
        user("普通用户");

        private final String desc;

        Sub(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }

        public static Sub fromDesc(String desc) {
            return Arrays.stream(Sub.values()).filter(f -> f.getDesc().equals(desc)).findFirst().orElse(null);
        }
    }
    public enum Client {
        pc("PC端"), app("APP");

        private final String desc;

        Client(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }

        public static Client fromDesc(String desc) {
            return Arrays.stream(Client.values()).filter(f -> f.getDesc().equals(desc)).findFirst().orElse(null);
        }
    }

    public boolean isUser() {
        return sub != null;
    }
}

