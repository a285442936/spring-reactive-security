package com.security.main.gateway.security.config;

import com.security.main.gateway.security.ApiReactiveAuthorizationManager;
import com.security.main.gateway.security.ApiServerAuthenticationSuccessHandler;
import com.security.main.gateway.security.authentication.JwtReactiveAuthenticationManager;
import com.security.main.gateway.security.converter.ServerJwtAuthenticationConverter;
import com.security.main.gateway.security.exception.ReactiveServerAccessDeniedHandler;
import com.security.main.gateway.security.exception.RxAuthenticationEntryPoint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;
import org.springframework.security.web.server.authentication.ServerAuthenticationEntryPointFailureHandler;

@EnableWebFluxSecurity
//@Import(Jwt)
@Slf4j
public class SecurityConfig {
    @Autowired
    private JwtReactiveAuthenticationManager jwtReactiveAuthenticationManager;
    @Autowired
    private ApiReactiveAuthorizationManager apiReactiveAuthorizationManager;


    @Bean
//    @DependsOn("defaultResdisService")
    public SecurityWebFilterChain webFluxSecurityFilterChain(ServerHttpSecurity http) {
        log.info("SecurityConfig webFluxSecurityFilterChain exec");
        http.csrf().disable()//禁用csrf
                .headers().disable()
                .httpBasic().disable()
                .formLogin().disable()
                .logout().disable()
                .requestCache().disable()
                .authorizeExchange()
//                .pathMatchers().permitAll()
                .pathMatchers(HttpMethod.OPTIONS).permitAll()
                .anyExchange().access(apiReactiveAuthorizationManager)
                .and()
                .exceptionHandling().authenticationEntryPoint(new RxAuthenticationEntryPoint())
                .accessDeniedHandler(new ReactiveServerAccessDeniedHandler())
                .and()
                .addFilterAt(jwtAuthenticationWebFilter(), SecurityWebFiltersOrder.HTTP_BASIC)
        ;

        return http.build();
    }

    private AuthenticationWebFilter jwtAuthenticationWebFilter() {
        AuthenticationWebFilter authenticationWebFilter = new AuthenticationWebFilter(jwtReactiveAuthenticationManager);
        authenticationWebFilter.setServerAuthenticationConverter(new ServerJwtAuthenticationConverter());
        authenticationWebFilter.setAuthenticationFailureHandler(new ServerAuthenticationEntryPointFailureHandler(new RxAuthenticationEntryPoint()));
        authenticationWebFilter.setAuthenticationSuccessHandler(new ApiServerAuthenticationSuccessHandler());
        return authenticationWebFilter;
    }
}
