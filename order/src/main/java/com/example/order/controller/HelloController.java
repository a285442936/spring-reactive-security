package com.example.order.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
//@RequestMapping("order")
@Slf4j
public class HelloController {

    @GetMapping("hello/{id}")
    public Mono<Map<String, String>> hello(@PathVariable Long id, ServerWebExchange exchange) {
        HttpHeaders headers = exchange.getRequest().getHeaders();
        for (Map.Entry<String, List<String>> entry : headers.entrySet()) {
            String key = entry.getKey();
            log.info("key:{},value:{}", key, entry.getValue());

        }
        HashMap<String, String> map = new HashMap<>();
        map.put("1, ", "22");
        return Mono.just(map);
    }

}
